![image](https://velog.velcdn.com/images/harryroh2003/post/f78adc37-4ca3-441e-8a2a-52594337e448/image.jpg)


# 🚀 Produto Spring Finance

Bem-vindo(a). Este é o Produto Spring Finance!

O objetivo deste produto é agilizar a sua vida financeira, com ferramentas para o controle financeiro que facilitam o seu dia-a-dia.

Neste produto vc pode optar em fazer o deploy com Docker-Compose ou Kubernetes.

# 🧠 Contexto

Tecnologias utilizadas neste produto:
-  Java v8 (v1.8)
-  Spring-Boot v2.7
-  Flyway (para as migrations)
-  JPA / Hibernate para a rotinas de banco de dados
-  Postgres
-  Pgadmin
-  Token JWT e Cors
-  JUnit v5 para os testes de integração
-  Pipelines End to End do backend
    - Na pipeline temos 4 jobs
        - run_migrations para validar se as migrations estão saudáveis e válidas
        - build para gerar o arquivo .jar da aplicação
        - test para rodar todos os testes de intregração da app
        - deploy para fazer o build e atualizar a imagem no docker-hub
-  React.JS
-  Javascript
-  Axios
-  Bootstrap
-  Pipeline End to End do frontend
    - Na pipeline temos 2 jobs
        - run_build_test para testar a fase de build da aplicação
        - deploy_batch_docker para fazer o build e atualizar a imagem no docker-hub
-  Docker
-  Kubernetes
-  Camada de Observabilidade
    -  Micrometer
    -  Actuator
    -  Prometheus
    -  Grafana
    -  AlertManager

# Video utilizando o produto
- link Utilizando o sistema (https://gitlab.com/marceloeduardo244/spring-finance-integracao/-/blob/main/docs/utilizando_o_sistema.mp4)

# Camada de Observabilidade da Aplicação
- A camada de Observabilidade serve para facilitar o contole e a gestão da saude das aplicaçoes.
- O Prometheus coleta as metricas da aplicação e armazena.
- O Grafana utiliza os dados do Prometheus para montar as metricas.
- E com o AlertManager vc pode configurar para que sejam observadas condições especificas da app, como se há conexões disponiveis com o database ou se o consumo de Cpu esta muito alto.
- Alem disso, o AlertManager pode mandar mensagens automaticamente para um canal no Slack, avisando sobre um possivel problema de forma autonoma.
- Para acessar o Grafana e visualizar o dashboard de status da App utilize o login abaixo
- user: admin
- password: 123456

# Video utilizando o dashboard de status da aplicação
- link Utilizando o sistema (https://gitlab.com/marceloeduardo244/spring-finance-integracao/-/blob/main/docs/utilizando_prometheus_com_grafana.mp4)

## Esquema de dados SQL do Produto
- https://gitlab.com/marceloeduardo244/spring-finance-integracao/-/blob/main/docs/estrutura_de_dados_do_produto.jpg

## Arquitetura Cluster Kubernetes
- https://gitlab.com/marceloeduardo244/spring-finance-integracao/-/blob/main/docs/arquitetura_do_cluster_kubernetes.jpg
- https://drive.google.com/file/d/1gDstyMx5rN5Jq0EoZcKL7HXkedBR9SpQ/view?usp=sharing

## Esquema de conexão do Docker compose
- https://gitlab.com/marceloeduardo244/spring-finance-integracao/-/blob/main/docs/arquitetura_da_stack_docker_compose.jpg
- https://drive.google.com/file/d/1o-7VC_zU-RzjzJv9pXv19MQKepEBBjxK/view?usp=sharing

# Backend do Produto
- https://gitlab.com/marceloeduardo244/spring-finance

# Frontend do Produto
- https://gitlab.com/marceloeduardo244/spring-finance-web

## 📋 Instruções para subir a aplicação utilizando Docker-compose

É necessário ter o Docker e Docker-compose instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- docker-compose up -d
- Vc terá novos containers rodando na sua maquina.
- Vc terá tbm o servidor proxy da aplicação, com os seguintes endereços
    - 80 - frontend
    - 9091 - pgadmin
    - 9092 - postgres
    - 9093 - backend
    - 9094 - prometheus
    - 9095 - grafana
    - 9096 - alertmanager

## 📋 Instruções para subir a aplicação utilizando o Kubernetes

É necessário ter o Docker e Docker-compose e Kubernets instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- kubectl apply -f ./kubernetes
- Vc terá novos containers rodando na sua maquina.
- Vc terá tbm o servidor proxy da aplicação, com os seguintes endereços
    - 80 - frontend
    - 9091 - pgadmin
    - 9092 - prometheus
    - 9093 - backend
    - 9094 - alertmanager
    - 9095 - grafana
- Para rodar o servidor execute os comandos abaixo na raiz do projeto
acesse o diretório ./devops/kubernetes/proxy-server/config
Edite o arquivo nginx.conf modificando o valor IPdaMAQUINA pelo ip da maquina onde cluster kubernetes foi iniciado.
- Para subir o container do proxy:
    - Acesse o diretório ./devops/kubernetes/proxy-server/
    - Execute o comando docker-compose up -d

Made with 💜 at OliveiraDev